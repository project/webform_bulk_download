<?php

/**
 * @file
 * Functions relating to Drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function webform_bulk_download_drush_command() {
  return array(
    'webform-bulk-download-export' => array(
      'description' => 'Process all webform bulk export requests. Exports webform data to files.',
    ),
    'webform-bulk-download-list' => array(
      'description' => 'Show how many bulk exports are in the queue.',
    ),
    'webform-bulk-download-gc' => array(
      'description' => 'Garbage collection, remove exported files older than 24 hrs.',
    ),
  );
}

/**
 * Exports a webform via drush.
 */
function drush_webform_bulk_download_export() {
  $queue = DrupalQueue::get('webform_bulk_download');
  $queue->createQueue();
  // Assume this will be get called every minute, we don't want to have too
  // many download jobs running at the same time.
  $max_concurrent_jobs = variable_get('webform_bulk_download_max_concurrent_jobs', 10);
  $number_running = _webform_bulk_download_get_nubmer_of_running_jobs();
  if ($number_running >= $max_concurrent_jobs) {
    drush_print("There are " . $number_running . " running items in the webform_bulk_download queue.");
    drush_print("TThat reaches the max (" . $max_concurrent_jobs . "). Wait until next time to execute the download job.");
    return;
  }

  // Claim item from the queue.
  $single_job_lease_time = 3600 * 2;
  $item = $queue->claimItem($single_job_lease_time);

  if ($item !== FALSE) {
    $mail_to = $item->data['mail_to'];
    $account = user_load_by_mail($mail_to);
    // Switch to the user who requested the export
    // so that webform_last_download table gets populated correctly.
    global $user;
    $orig_user = $user;
    $user = $account;
    $result = _webform_bulk_download_export_one($item);
    // Switch back to real user.
    $user = $orig_user;
    $params['account']  = $account;
    drush_print('Sending email to user ' . $account->uid . ' with link to download submission from node ' . $item->data['nid'] . '.');
    $mail_language = user_preferred_language($params['account']);
    if ($result) {
      // Email success message.
      drush_print('Email with success message.');
      $params['file_name'] = $result;
      drupal_mail('webform_bulk_download', 'success', $mail_to, $mail_language, $params);
    }
    else {
      // Email failed message.
      drush_print('Email with fail message.');
      drupal_mail('webform_bulk_download', 'fail', $mail_to, $mail_language, $params);
    }
    _webform_bulk_download_delete_queue_item($item);
  }

}

/**
 * List how many items are in the queue.
 */
function drush_webform_bulk_download_list() {
  // For some reason we have to clone the result, otherwise it gets unset.
  $queue = clone DrupalQueue::get('webform_bulk_download', TRUE);
  $number = $queue->numberOfItems();
  drush_print("There are " . $number . " items (new or running) in the webform_bulk_download queue.");
  $number_running = _webform_bulk_download_get_nubmer_of_running_jobs();
  drush_print("There are " . $number_running . " running items in the webform_bulk_download queue.");
}

/**
 * Delete older files.
 */
function drush_webform_bulk_download_gc() {
  _webform_bulk_download_gc();
}
